import requests
import json
import re   
import pandas as pd

with open('stat.json', mode='wb') as localfile:
    limit = 199
    offset = 0
    url = "https://data.dol.gov/get/SweatToilAllStatistics?limit="+str(limit)+"&offset="+str(offset)
    headers = {'x-api-key' : '9ff8f0c3-a7c5-461c-9294-b0ecae52f93d'}
    resp = requests.get(url,headers=headers)
    while resp:
        print(offset)
        resp = requests.get(f"https://data.dol.gov/get/SweatToilAllStatistics?limit="+str(limit)+"&offset=" +str(offset), headers=headers)
        
        offset += limit
        print(resp.content=='')
        if (resp.content==''):
            break
        localfile.write(resp.content)
        resp = requests.get(f"https://data.dol.gov/get/SweatToilAllStatistics?limit="+str(limit)+"&offset=" +str(offset), headers=headers)
        

with open('countrygoods.json', mode='wb') as localfile2:
    limit = 199
    offset = 0
    url = "https://data.dol.gov/get/SweatToilAllCountryGoods?limit="+str(limit)+"&offset="+str(offset)
    headers = {'x-api-key' : '9ff8f0c3-a7c5-461c-9294-b0ecae52f93d'}
    resp = requests.get(url,headers=headers)
    while resp:
        print(offset)
        resp = requests.get(f"https://data.dol.gov/get/SweatToilAllCountryGoods?limit="+str(limit)+"&offset=" +str(offset), headers=headers)
        offset += limit
        print(resp.content=='')
        if (resp.content==''):
            break
        localfile2.write(resp.content)
        resp = requests.get(f"https://data.dol.gov/get/SweatToilAllCountryGoods?limit="+str(limit)+"&offset=" +str(offset), headers=headers)





stat = open("stat.json")
lines = stat.readlines()
a=len(lines)
#for i in range((len(lines))-3):
i = 0
while i < a-3 :

    lines[i].strip()
    if ("}" in lines[i]):
        if("][" in lines[i+1]):
            if("{" in lines [i+2]):

                lines[i]=lines[i].replace('}','},')
                del lines[i+1]

                a-=1
    i+=1
for i in range(0,len(lines)):
    lines[i]=lines[i].strip()
    
stat.close()

'''
Il ne faut garder que les assessment id uniques
'''

assessment = []
for i in range(3,len(lines),18):
    assessment.append(i)
    
        
i_unique = []
assessment_unique = []
for i in assessment:
    if lines[i] not in assessment_unique:
        i_unique.append(i)
        assessment_unique.append(lines[i])
        
'''
une fois que l on connais les lignes qu il faut garder, il faut garder les lignes (i-2)-> (i+15)
'''
i = 3
for i in reversed(range(3,len(lines),18)):
    if not  i in i_unique:
        print(i)
        del lines[i-2:i+16]
        
        
    
with open('stat_new.json', 'w') as f:
    for line in lines:
        f.write(line + '\n')

lines_copy = lines.copy()
for i in range(0,len(lines_copy)):
    lines_copy[i]=lines_copy[i].replace('"','')
    
'''
Supprimer les donnees si :
    percent_of_working_children_agriculture: xx.x,
    percent_of_working_children_industry: xx.x,
    percent_of_working_children_services: xx.x,
'''

for i in reversed(range(10,len(lines_copy),18)):
    if (((lines_copy[i] == 'percent_of_working_children_agriculture: xx.x,') & (lines_copy[i+1] == 'percent_of_working_children_industry: xx.x,') & (lines_copy[i+2] == 'percent_of_working_children_services: xx.x,')) or ((lines_copy[i] == 'percent_of_working_children_agriculture: Unavailable,') & (lines_copy[i+1] == 'percent_of_working_children_industry: Unavailable,') & (lines_copy[i+2] == 'percent_of_working_children_services: Unavailable,')) or ((lines_copy[i] == 'percent_of_working_children_agriculture: ,') & (lines_copy[i+1] == 'percent_of_working_children_industry: ,') & (lines_copy[i+2] == 'percent_of_working_children_services: ,'))) :
    
    #| (lines_copy[i-1] =='population_of_working_children: Unavailable,')):
        print(i)
        del lines_copy[i-9:i+9]
    
for i in reversed(range(9,len(lines_copy),18)):
    if (lines_copy[i] == 'population_of_working_children: Unavailable,') or (lines_copy[i] == 'population_of_working_children: unavailable,')or (lines_copy[i] == 'population_of_working_children: N\/A,'):
        print(i)
        del lines_copy[i-8:i+10]

with open('remove_non_needed_data.json', 'w') as f:
    for line in lines_copy:
        f.write(line + '\n')
        


column_names = ['Country','Year','Name','Value']
table_child_labor = pd.DataFrame(columns = column_names)

for i in range(6,len(lines_copy),18):
    country = (re.search('country: (.*),', lines_copy[i])).group(1)
    if country == 'C\u00c3\u00b4te d\u00e2\u0080\u0099Ivoire' :
        country = 'Republic of Côte d Ivoire'
    
    print(country)
    year = (re.search('year: (.*),', lines_copy[i-2])).group(1)
    print(year)
    population = (re.search('population_of_working_children: (.*),',lines_copy[i+3])).group(1)
    if not ((population == '')):
        if ',' in population:
            population=int(population.replace(',',''))
#        if '.' in population:
#            print('point',population)
        else :
            if '.' in population:
                population=round(float(population))
            else :
                population = int(population)
        
        #if (re.search('population_of_working_children: (.*)',lines_copy[i+3])).group(1) =='.0':
            
        if not ((re.search('percent_of_working_children_agriculture: (.*),',lines_copy[i+4]).group(1)=='') or (re.search('percent_of_working_children_agriculture: (.*),',lines_copy[i+4]).group(1)=='Unavailable')) :
            pourcent_agriculture = float((re.search('percent_of_working_children_agriculture: (.*),',lines_copy[i+4])).group(1))
        else :
            pourcent_agriculture = None
        
        if not ((re.search('percent_of_working_children_industry: (.*),',lines_copy[i+5]).group(1)=='')or (re.search('percent_of_working_children_industry: (.*),',lines_copy[i+5]).group(1)=='Unavailable')) :
            pourcent_industry = float((re.search('percent_of_working_children_industry: (.*),',lines_copy[i+5])).group(1))
        else :
            pourcent_industry = None
        
        if not ((re.search('percent_of_working_children_services: (.*),',lines_copy[i+6]).group(1)=='')or (re.search('percent_of_working_children_services: (.*),',lines_copy[i+6]).group(1)=='Unavailable')) :
            pourcent_service = float((re.search('percent_of_working_children_services: (.*),',lines_copy[i+6])).group(1))
        else :
            pourcent_service = None
        
        if not ( pourcent_agriculture is None and pourcent_industry is None and pourcent_service is None):
                
            if (pourcent_agriculture+pourcent_industry+pourcent_service >90):
            
                agriculture = float((re.search('percent_of_working_children_agriculture: (.*),',lines_copy[i+4])).group(1)) * (population / 100)
                table_child_labor = table_child_labor.append(pd.Series([country,year,'number_of_working_children_agriculture',agriculture], index=['Country','Year','Name','Value']),ignore_index=True)
      
                industrie = float((re.search('percent_of_working_children_industry: (.*),',lines_copy[i+5])).group(1)) * (population / 100)
                table_child_labor = table_child_labor.append(pd.Series([country,year,'number_of_working_children_industry',industrie], index=['Country','Year','Name','Value']),ignore_index=True)
    
                service = float((re.search('percent_of_working_children_services: (.*),',lines_copy[i+6])).group(1)) * (population / 100)
                table_child_labor = table_child_labor.append(pd.Series([country,year,'number_of_working_children_services',service], index=['Country','Year','Name','Value']),ignore_index=True)
            
            else  :
                if (pourcent_agriculture+pourcent_industry+pourcent_service <5):
                    agriculture = float((re.search('percent_of_working_children_agriculture: (.*),',lines_copy[i+4])).group(1)) * (population)
                    table_child_labor = table_child_labor.append(pd.Series([country,year,'number_of_working_children_agriculture',agriculture], index=['Country','Year','Name','Value']),ignore_index=True)
          
                    industrie = float((re.search('percent_of_working_children_industry: (.*),',lines_copy[i+5])).group(1)) * (population)
                    table_child_labor = table_child_labor.append(pd.Series([country,year,'number_of_working_children_industry',industrie], index=['Country','Year','Name','Value']),ignore_index=True)
        
                    service = float((re.search('percent_of_working_children_services: (.*),',lines_copy[i+6])).group(1)) * (population)
                    table_child_labor = table_child_labor.append(pd.Series([country,year,'number_of_working_children_services',service], index=['Country','Year','Name','Value']),ignore_index=True)


table_child_labor_pivot = table_child_labor.pivot_table(index=['Country','Name'], columns='Year', values='Value')                


countrygoods = open("countrygoods.json")
lines_goods = countrygoods.readlines()
b=len(lines_goods)

i = 0
while i < b-3 :

    lines_goods[i].strip()
    if ("}" in lines_goods[i]):
        if("][" in lines_goods[i+1]):
            if("{" in lines_goods [i+2]):

                lines_goods[i]=lines_goods[i].replace('}','},')
                del lines_goods[i+1]

                b-=1
    i+=1
for i in range(0,len(lines_goods)):
    lines_goods[i]=lines_goods[i].strip()
    
countrygoods.close()

lines_goods_copy = lines_goods.copy()
for i in range(0,len(lines_goods_copy)):
    lines_goods_copy[i]=lines_goods_copy[i].replace('"','')
    
    
column_names = ['Country','Year','Good','Child labor','Forced labor','Forced child labor']
table_country_goods = pd.DataFrame(columns = column_names)

for i in range(6,len(lines_goods_copy),13):
    country = (re.search('country: (.*),', lines_goods_copy[i])).group(1)

    year = (re.search('year: (.*),', lines_goods_copy[i-2])).group(1)
    good = (re.search('good: (.*),',lines_goods_copy[i+2])).group(1)
    child_labor = (re.search('child_labor: (.*),', lines_goods_copy[i+4])).group(1)    
    forced_labor = (re.search('forced_labor: (.*),', lines_goods_copy[i+5])).group(1)    
    forced_child_labor = (re.search('forced_child_labor: (.*)', lines_goods_copy[i+6])).group(1)    
    
    
    table_country_goods = table_country_goods.append(pd.Series([country,year,good,child_labor,forced_labor,forced_child_labor], index=['Country','Year','Good','Child labor','Forced labor','Forced child labor']),ignore_index=True)
    table_country_goods.to_csv('table_country_goods.csv',index=False)
    


               